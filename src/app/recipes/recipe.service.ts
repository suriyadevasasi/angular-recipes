import { Recipe } from './recipe.model';
import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();


  private recipes: Recipe[] = [
    new Recipe('Ratatouille Casserole',
     'Ratatouille is a French Provençal stewed vegetable dish',
     'https://media.chefdehome.com/740/0/0/ratatouille/ratatouille-casserole.jpg',
     [
       new Ingredient('EggPlants', 2),
       new Ingredient('Roma Tomatoes', 6),
       new Ingredient('Yellow Squashes', 2),
       new Ingredient('Zucchinis', 2)
     ]),
    new Recipe('Nasi Lemak',
     'Nasi lemak is a Malay cuisine dish consisting of fragrant rice cooked in coconut milk and pandan leaf.',
     'https://www.huangkitchen.com/wp-content/uploads/2016/03/IMG_4118_new.jpg',
     [
      new Ingredient('Boiled Egg', 2),
      new Ingredient('Groundnuts', 1),
      new Ingredient('Anchovies', 1),
      new Ingredient('Cucumber', 1)
     ])
  ];

   //private recipes: Recipe[] = [];

  constructor(private slService: ShoppingListService){ }

  setRecipes(recipes: Recipe[] ) {
    
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes(){
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]){
    this.slService.addIngredients(ingredients);
  }
  addRecipe(recipe: Recipe){
this.recipes.push(recipe);
this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe){
this.recipes[index] = newRecipe;
this.recipesChanged.next(this.recipes.slice());

  }
  deleteRecipe(index: number){
this.recipes.splice(index, 1);
this.recipesChanged.next(this.recipes.slice());
  }
}
